#! /usr/bin/env python
from __future__ import print_function
import rospy
# Brings in the SimpleActionClient
import actionlib
import speech_recognition as sr
import os

# Brings in the messages used by the speech action, including the
# goal message and the result message.
import speech2txt_action.msg

def speech2txt_client():
    client = actionlib.SimpleActionClient('speech', speech2txt_action.msg.SpeechAction)
     
           # Waits until the action server has started up and started
           # listening for goals.
    client.wait_for_server()
    #this command records the audio and saves it to speech_to_text file.it is a linux command
    print  ("Saysomething") 
    os.system("ffmpeg -f pulse -i default speech_to_text.wav")       
    AUDIO_FILE = ("speech_to_text.wav")
    # use the audio file as the audio source
    r = sr.Recognizer()
    with sr.AudioFile(AUDIO_FILE) as source:
    #reads the audio file. Here we use record instead of listen     #function
         audio = r.record(source)  
    y=r.recognize_google(audio)
    
           # Creates a goal to send to the action server.
           # Creates the SimpleActionClient, passing the type of the action
           # (SpeechAction) to the constructor.

    goal = speech2txt_action.msg.SpeechGoal(text=y)
           #feedback=speech2txt_action.msg.SpeechFeedback(sequence=y)
           #client.send_feedback(feedback)
           # Sends the goal to the action server.
    client.send_goal(goal)

    

           # Waits for the server to finish performing the action.
    client.wait_for_result()

           # Prints out the result of executing the action
    return client.get_result() 
    

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('speech_client')
        result = speech2txt_client()
        print("Result:",result.sequence)
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)

